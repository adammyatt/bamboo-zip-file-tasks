package com.pronetbeans.bamboo.filezip;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.*;
import com.atlassian.core.util.FileUtils;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * Bamboo Task for extracting zip files.
 *
 * @author Adam Myatt
 */
public class FileExtractZipTask implements TaskType {

    private static final Logger log = Logger.getLogger(FileExtractZipTask.class);

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {

        final BuildLogger buildLogger = taskContext.getBuildLogger();
        final String propsZipLocation = taskContext.getConfigurationMap().get("propsZipLocation");
        final String propsExtractLocation = taskContext.getConfigurationMap().get("propsExtractLocation");

        boolean itWorked = false;
        ZipInputStream instream = null;

        try {
            byte[] buf = new byte[1024];
            instream = new ZipInputStream(new BufferedInputStream(new FileInputStream(new File(taskContext.getWorkingDirectory(), propsZipLocation))));

            String destinationDir = null;

            if (propsExtractLocation != null && propsExtractLocation.trim().length() > 0) {
                destinationDir = taskContext.getWorkingDirectory().getAbsolutePath() + File.pathSeparator + propsExtractLocation;
            } else {
                destinationDir = taskContext.getWorkingDirectory().getAbsolutePath();
            }
            
            ZipEntry zentry = instream.getNextEntry();

            while (zentry != null) {
                String entryName = zentry.getName();
                FileOutputStream outstream = null;

                try {
                    File newFile = new File(destinationDir, entryName);
                    if (!newFile.exists()) {
                        if (!newFile.getParentFile().exists()) {
                            newFile.getParentFile().mkdirs();
                        }

                    } else {
                        if (!newFile.delete()) {
                            buildLogger.addBuildLogEntry(newFile.getName() + " is unable to be deleted.");
                        }
                    }

                    if (!zentry.isDirectory()) {

                        outstream = new FileOutputStream(newFile);
                        int n;

                        while ((n = instream.read(buf, 0, buf.length)) > -1) {
                            outstream.write(buf, 0, n);
                        }
                    }
                } catch (Exception e) {
                    log.info(Utils.getLogBanner());
                    buildLogger.addErrorLogEntry("Error extracting an entry from the zip file and writing it to disk : " + e.getMessage(), e);
                } finally {
                    try {
                        if (outstream != null) {
                            outstream.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                instream.closeEntry();
                zentry = instream.getNextEntry();
            }

            itWorked = true;

        } catch (Exception e) {
            log.info(Utils.getLogBanner());
            buildLogger.addErrorLogEntry("Error extracting from zip file : " + e.getMessage(), e);
        } finally {

            try {
                if (instream != null) {
                    instream.close();
                    instream = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (itWorked) {
            return TaskResultBuilder.create(taskContext).success().build();
        } else {
            return TaskResultBuilder.create(taskContext).failedWithError().build();
        }
    }
}